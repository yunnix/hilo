import java.util.Scanner;

public class HiLo {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String playAgain = "";

		do {
			//inisialisasi angka acak untuk ditebak
			int theNumber = (int)(Math.random() * 100 + 1);
			int guess = 0;
			while (guess != theNumber) {
				System.out.println("Tebak sebuah angka antara 1 - 100:");
				guess = scan.nextInt();
				if (guess < theNumber)
					System.out.println(guess + " terlalu kecil, tebak lagi!");
				else if (guess > theNumber)
					System.out.println(guess + " terlalu besar, tebak lagi!");
				else
					System.out.println(guess + " adalah benar, anda menang!");
			} // while block
			System.out.println("mau main lagi ? (y/n)");
			playAgain = scan.next();
		} while (playAgain.equalsIgnoreCase("y"));
		System.out.println("Terima Kasih sudah bermain, sampai jumpa !")
		scan.close();
	}
}